function factotielle(n) {
    if (n === 1) {
        return 1;
    } else {
        return n * factotielle(n - 1);
    }
}

// console.log(factotielle(5))

function fibonacci(n) {
    if (n <= 1) {
        return n;
    }
    return fibonacci(n - 1) + fibonacci(n - 2); // si 'n' > 1,
}

// console.log(fibonacci(4))

function syracuse(n) {
    console.log(n);
    if (n === 1) {
        return 1;
    } else if (n % 2 === 0) {    // si 'n' est pair
       return syracuse(n / 2);  // 'n' est divisé par 2
    } else {
       return syracuse(3 * n + 1); // si 'n' est impair, 'n' est multiplié par 3 et on lui ajoute 1
    }
}

console.log(syracuse(15))

