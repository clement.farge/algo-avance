let tab = [2, 5, 7, 1, 3, 6, 4, 0]
console.table(tab)

function swap(tab) {
    for (let i = 0; i < tab.length; i++) {
        if (tab[i] > tab[i + 1]) {
            let j = tab[i];
            tab[i] = tab[i + 1];
            tab[i + 1] = j;
            console.table(tab);
        }
    }
}

// swap(tab, 0, 1);


function InsertionSort(tab) {
    for (let i = 1; i < tab.length; i++) {
        let current = tab[i];
        let j = i - 1;
        while ((j > -1) && ( current < tab[j])) {
            tab[j + 1] = tab[j];
            j--;
        }
        tab[j + 1] = current;
    }
    console.table(tab)
}

// InsertionSort(tab);


function selectionSort(tab) {
    for (let i = 0; i < tab.length; i++) {
        let min = i;
        for (let j = i + 1; j < tab.length; j++) {
            if (tab[j] < tab[min]) {
                min = j;
            }
        }
        swap(tab)
    }
    console.table(tab);
}

// selectionSort(tab)


function bubbleSort(tab) {
    for (let i = 0; i < tab.length; i++) {
        for(let j = 1 ; j < tab[i + 1] ; j++){
            if (tab[i] > tab[j]){
                swap(tab);
            }
        }
    }
}

// bubbleSort(tab)

