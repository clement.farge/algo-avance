function InsertionSort(lst) {
    let size = lst.length;
    for (let i = 1; i < size; i++) {
        let current = lst[i];
        let j = i-1;
        while ((j > -1) && (current < lst[j])) {
            lst[j+1] = lst[j];
            j--;
        }
        lst[j+1] = current;
    }
    return lst;
}

let tab = [8,3,6,4,2,7,5,9,1]
console.table(tab)

InsertionSort(tab);
console.table(tab)
